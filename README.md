# 🐕 SP SquonK 🐕

ランダムな中国語の記号から始めましょう。

*My discord username*: spsquonk

***All my hobby projects are currently in limbo, except Esthar.***

## Triple Triad

[I am the developer and administrator of Esthar, a FFWTT private server 🇫🇷](https://esthar.fr), a french online Triple Triad game. (since 2021-07)

## Pokémon

I play pokemon fangames, including the official series. I kind of got interested in the fan games because I kind of hate the current direction the official license is taking. I list most of my playthoughts [on my website 🇬🇧](https://squonk.fr/pokemon/), and post poorly written reviews on gist.

## FlyFF

I am a FlyFF private server developer that put most of his energy into refactoring the code and the architecture.

**I do not sell systems, exploits, ...**

Some of my works include :
- My [Server Starter 🇬🇧](https://gitlab.com/SPSquonK/ServerStarter)
- [I fused every server side executable for SFlyFF](https://www.xn--s-sfa.fr/wp-content/uploads/2020/05/DBWorldServer02.png)
- I used to post snippets on my [gist page 🇬🇧](https://gist.github.com/SPSquonK/). I do not know if I will continue to do this on Gitlab and where.
- I write a french [devblog 🇫🇷](https://www.sà.fr) about my adventures (warning: because my writing skills are bad, you need a MSc both in FlyFF and C++ to understand something)

My three projects are/were:
- SFlyFF 1, 2014: A two weeks project when I knew nothing about C++.
- SFlyFF 2, 2019~2021-01: [https://sflyff.fr 🇫🇷](https://sflyff.fr) exposes the list of features that I've implemented (and that can be seen from the players' perspective)
- useleSS, since 2022-04: [on GitLab 🇬🇧](https://gitlab.com/SPSquonK/useleSS) a project where I just refactor things in FlyFF v15 source.

